from __future__ import print_function

import os
import sys
import base64
import time

from st2common.runners.base_action import Action

import tcms_api
from tcms_api.xmlrpc import TCMSXmlrpc


class KiwiTCMSBaseAction(Action):

    APIMAP = {
        "classification.get": "getClassification",
        "product.ensure": "ensureProduct",
        "version.ensure": "ensureVersion",
        "build.ensure": "ensureBuild",
        "plantype.ensure": "ensurePlanType",
        "testplan.ensure": "ensureTestPlan",
        "testcase.ensure": "ensureTestCase",
        "testrun.create": "createTestRun",
        "testrun.update": "updateTestRun",
        "testrun.addcase": "addTestCaseToTestRun",
        "testrun.addattachment": "addAttachmentToTestRun",
        "testexecution.update": "updateTestExecution",
        "testrun.createFromPlan": "createTestRunFromPlan",
    }

    def __init__(self, config):
        super(KiwiTCMSBaseAction, self).__init__(config)

        self.debug = config.get("debug", False)
        self.tcms = TCMSXmlrpc(config.get("username"),config.get("password"),
                               config.get("url")).server

    def run(self, **kwargs):
        action_name = kwargs.pop("action")

        if not action_name in self.APIMAP:
            print("Unknown action %r",action_name,file=sys.stderr)
            exit(1)

        return getattr(self,self.APIMAP[action_name])(kwargs)

    def getClassification(self, params):
        classifications = self.tcms.Classification.filter(dict(name=params.get("name")))
        if not classifications:
            print("Unknown kiwitcms classification %r; must create manually",
                  params.get("name"),file=sys.stderr)
            exit(1)
        return classifications[0]

    def ensureProduct(self, params):
        real_params = dict()
        for (k,v) in params.items():
            if k in [ "name", "description", "classification" ]:
                real_params[k] = v
        product = self.tcms.Product.filter(real_params)
        if product:
            return product[0]
        return self.tcms.Product.create(real_params)

    def ensureVersion(self, params):
        real_params = dict()
        for (k,v) in params.items():
            if k in [ "value", "product" ]:
                real_params[k] = v
        version = self.tcms.Version.filter(real_params)
        if version:
            return version[0]
        return self.tcms.Version.create(real_params)

    def ensureBuild(self, params):
        real_params = dict()
        for (k,v) in params.items():
            if k in [ "name", "version" ]:
                real_params[k] = v
        build = self.tcms.Build.filter(real_params)
        if build:
            return build[0]
        return self.tcms.Build.create(real_params)

    def ensurePlanType(self, params):
        real_params = dict(name=params.get("name"))
        if "description" in params:
            real_params["description"] = params.get("description")
        plantype = self.tcms.PlanType.filter(real_params)
        if plantype:
            return plantype[0]
        return self.tcms.PlanType.create(real_params)

    def ensureTestPlan(self, params):
        product_version = params.get("product_version", None)
        if product_version == None:
            product_version = self.tcms.Version.filter(dict(product_id=params.get("product"),value="unspecified"))[0]["id"]
        if not product_version:
            print("Cannot find a product version for %r/%r; must create",
                  params.get("product"),params.get("product_version",None),file=sys.stderr)
            exit(1)

        real_params = dict()
        for (k,v) in params.items():
            if k in [ "name", "text", "type", "product" ]:
                real_params[k] = v
        real_params["product_version"] = product_version
        testplan = self.tcms.TestPlan.filter(real_params)
        if testplan:
            testplan = testplan[0]
        else:
            testplan = self.tcms.TestPlan.create(real_params)

        # XXX Hack to "convert" xmlrpc.client.DateTime to something json.dumps can swallow.
        if "create_date" in testplan:
            testplan["create_date"] = str(testplan["create_date"])

        return testplan

    def ensureTestCase(self, params):
        case_status = self.tcms.TestCaseStatus.filter(dict(name=params.get("status")))
        if not case_status:
            print("Cannot find a test case status for %r; must create",
                  params.get("status"),file=sys.stderr)
            exit(1)

        category = self.tcms.Category.filter(
            dict(name=params.get("category"),product_id=params.get("product")))
        if not category:
            print("Cannot find a category for %r (product_id %r); must create",
                  params.get("category"),params.get("product"),file=sys.stderr)
            exit(1)

        priority = self.tcms.Priority.filter(dict(value=params.get("priority")))
        if not priority:
            print("Cannot find a priority for %r; must create",
                  params.get("priority"),file=sys.stderr)
            exit(1)

        real_params = dict(case_status=case_status[0]["id"],category=category[0]["id"],
                           priority=priority[0]["id"])
        for (k,v) in params.items():
            if k in [ "summary", "text" ]:
                real_params[k] = v
        testcase = self.tcms.TestCase.filter(real_params)
        if testcase:
            testcase = testcase[0]
        else:
            testcase = self.tcms.TestCase.create(real_params)

        if "create_date" in testcase:
            testcase["create_date"] = str(testcase["create_date"])

        # Optionally add to a test plan
        if params.get("testplan",None) is not None:
            self.tcms.TestPlan.add_case(params.get("testplan"),testcase["id"])
            planlink = "planID:" + str(params.get("testplan"))
            self.tcms.TestCase.update(testcase["id"], dict(extra_link=planlink))

        return testcase

    def createTestRun(self, params):
        real_params = dict()
        for (k,v) in params.items():
            if k in [ "summary", "build", "plan", "manager", "start_date", "notes" ]:
                if k == "start_date":
                    if params[k]:
                        real_params[k] = str(v)
                else:
                    real_params[k] = v

        if "start_date" not in real_params:
            now = time.strftime("20%y-%m-%dT%H:%M:%SZ", time.gmtime())
            real_params["start_date"] = now

        testrun = self.tcms.TestRun.create(real_params)
        if testrun and "tags" in params and params["tags"]:
            for t in params["tags"]:
                self.tcms.TestRun.add_tag(testrun["id"],t)
        
        # XXX Hack to "convert" xmlrpc.client.DateTime to something json.dumps
        # can swallow.
        if "start_date" in testrun:
            testrun["start_date"] = str(testrun["start_date"])

        return testrun

    def updateTestRun(self, params):
        real_params = dict()
        for (k,v) in params.items():
            if k in [ "summary", "notes", "start_date", "stop_date" ]:
                real_params[k] = v
        testrun = self.tcms.TestRun.update(params.get("run"),real_params)
        # XXX Hack to "convert" xmlrpc.client.DateTime to something json.dumps
        # can swallow.
        for k in [ "start_date", "stop_date" ]:
            if k in testrun:
                testrun[k] = str(testrun[k])
        return testrun

    def createTestRunFromPlan(self, params):
        #print(str(params),file=sys.stderr)
        #print("params",file=sys.stderr)
        
        real_params = dict()
        #
        # See if we can find a plan that matches
        #
        real_params["name"] = params["plan_name"]
        real_params["type__name"] = params["plan_type"]
        real_params["product__name"] = params["product_name"]
        real_params["product_version__value"] = params["product_version"]

        testplan = self.tcms.TestPlan.filter(real_params)
        if not testplan:
            return None

        #print("testplan",file=sys.stderr)
        #print(str(testplan),file=sys.stderr)        

        # We need the build to create a run.
        real_params = dict()
        real_params["name"] = params["product_name"]
        real_params["classification__name"] = params["classification"]
        product = self.tcms.Product.filter(real_params)
        #print("product",file=sys.stderr)
        #print(str(product),file=sys.stderr)

        real_params = dict()
        real_params["value"] = params["product_version"]
        real_params["product"] = product[0]["id"]
        version = self.tcms.Version.filter(real_params)
        #print("version",file=sys.stderr)

        real_params = dict()
        real_params["name"] = params["build"]
        real_params["version"] = version[0]["id"]
        build = self.tcms.Build.filter(real_params)
        #print("build",file=sys.stderr)
        #print(str(build),file=sys.stderr)

        #
        # Grab the test cases for the plan. Truns out the RPC interface
        # does not support getting the testcases directly from the plan.
        # So have search for testcases with the planID:X set in the
        # extra_link field (see above).
        #
        real_params = dict()
        real_params["plan"] = testplan[0]["id"]
        real_params["extra_link"] = "planID:" + str(testplan[0]["id"])
        testcases = self.tcms.TestCase.filter(real_params)
        if not testcases:
            return None
        #print("testcases",file=sys.stderr)
        #print(str(testcases),file=sys.stderr)

        real_params = dict()
        real_params["summary"] = params["summary"]
        real_params["plan"] = testplan[0]["id"]
        real_params["build"] = build[0]["id"]
        real_params["manager"] = "admin"
        now = time.strftime("20%y-%m-%dT%H:%M:%SZ", time.gmtime())
        real_params["start_date"] = now

        #print("testrun params",file=sys.stderr)
        #print(str(real_params),file=sys.stderr)
        testrun = self.tcms.TestRun.create(real_params)
        if not testrun:
            return None

        # XXX Hack to "convert" xmlrpc.client.DateTime to something json.dumps
        # can swallow.
        if "start_date" in testrun:
            testrun["start_date"] = str(testrun["start_date"])

        # Add testcases to the run.
        if testcases:
            testrun["testcases"] = dict()
            
            for testcase in testcases:
                case_params = dict()
                case_params["run"] = testrun["id"]
                case_params["case"] = testcase["id"]
                case_params["status"] = "BLOCKED"
                ret = self.addTestCaseToTestRun(case_params)
                testrun["testcases"][testcase["summary"]] = ret["id"]
                pass
            pass

        #print("testrun",file=sys.stderr)
        #print(str(testrun),file=sys.stderr)
        return testrun

    def addTestCaseToTestRun(self, params):
        ret = self.tcms.TestRun.add_case(params.get("run"),params.get("case"))[0]
        eid = ret["id"]
        if params.get("status", None):
            self.updateTestExecution(dict(execution=eid, status=params.get("status")))
        return ret

    def addAttachmentToTestRun(self, params):
        content = params.get("content")
        try:
            base64.b64decode(content, validate=True)
        except:
            content = base64.b64encode(content.encode("utf8")).decode("utf8")
        return self.tcms.TestRun.add_attachment(params.get("run"),params.get("filename"),content)

    def updateTestExecution(self, params):
        if params.get("comment", None) != None:
            self.tcms.TestExecution.add_comment(
                params.get("execution"),params.get("comment"))
        status = self.tcms.TestExecutionStatus.filter(dict(name=params.get("status")))[0]
        real_params = dict(status=status["id"])
        for (k,v) in params.items():
            if k in [ "actual_duration" ]:
                real_params[k] = v
        return self.tcms.TestExecution.update(params.get("execution"),real_params)
